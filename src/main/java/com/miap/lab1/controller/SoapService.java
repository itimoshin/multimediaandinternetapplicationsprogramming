package com.miap.lab1.controller;

import com.miap.lab1.entity.PhoneBookItem;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.List;

public class SoapService extends WebServiceGatewaySupport {


    public List<PhoneBookItem> getList(String ticker) {

        GetQuote request = new GetQuote();
        request.setSymbol(ticker);

        log.info("Requesting quote for " + ticker);

        GetQuoteResponse response = (GetQuoteResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://www.webservicex.com/stockquote.asmx",
                        request,
                        new SoapActionCallback("http://www.webserviceX.NET/GetQuote"));

        return response;
    }

}