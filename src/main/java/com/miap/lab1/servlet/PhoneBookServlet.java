package com.miap.lab1.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.service.PhoneBookService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/phoneBook","/phoneBook/*"} )
public class PhoneBookServlet extends HttpServlet{

    private PhoneBookService phoneBookService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<PhoneBookItem> result = phoneBookService.filter(req.getParameter("firstName"), req.getParameter("lastName"));
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(objectMapper.writeValueAsString(result));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PhoneBookItem result = phoneBookService.add(objectMapper.readValue(req.getInputStream(), PhoneBookItem.class));
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(objectMapper.writeValueAsString(result));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSegments = req.getRequestURI().split("/");
        phoneBookService.delete(Long.valueOf(uriSegments[uriSegments.length-1]));
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ApplicationContext ac = (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        this.phoneBookService = ac.getBean(PhoneBookService.class);
    }
}
