package com.miap.lab1.dao;

import com.miap.lab1.entity.PhoneBookItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public interface PhoneBookRepository /*extends JpaRepository<PhoneBookItem, Long>*/{
    PhoneBookItem add(PhoneBookItem phoneBookItem);

    @SuppressWarnings("unchecked")
    List<PhoneBookItem> filter(String firstName, String lastName);

    void delete(long id);
}
