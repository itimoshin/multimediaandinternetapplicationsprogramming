package com.miap.lab1.ui.applet;

import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.ui.EditPhoneBookModal;
import com.miap.lab1.ui.PhoneBookTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PhoneBookApplet extends JApplet {

    public static final String APP_ENDPOINT = "http://localhost:8080/miap/phoneBook";

    private final PhoneBookTable table = new PhoneBookTable();


    @Override
    public void init() {
        try {
            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    render();
                }
            });
        } catch (Exception e) {
            System.err.println("createGUI didn't successfully complete");
        }
    }

    public PhoneBookApplet() throws HeadlessException {
        super();
        this.setLayout(new GridBagLayout());
    }

    private void render() {
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;c.weightx = 0.5; c.gridy = 0;
        JButton refreshButton = new JButton("Обновить");
        refreshButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                table.init();
            }
        });
        add(refreshButton, c);

        JButton addButton = new JButton("Добавить");
        JApplet window = this;
        addButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                EditPhoneBookModal editPhoneBookModal = new EditPhoneBookModal((JFrame) SwingUtilities.windowForComponent(window), new PhoneBookItem());
                editPhoneBookModal.addWindowStateListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        table.add(editPhoneBookModal.getSavedItem());
                    }
                });
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;c.weightx = 0.5; c.gridy = 0;
        add(addButton,c);

        JButton removeButton = new JButton("Удалить");
        removeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(table.getSelectedRow()>=0)
                    table.removeByIndex(table.getSelectedRow());
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;c.weightx = 0.5; c.gridy = 0;
        add(removeButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.weightx = 2;
        c.gridwidth = 3;
        c.gridy = 1;
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane,c);
        setVisible(true);
    }
}
