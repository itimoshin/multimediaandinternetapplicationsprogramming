package com.miap.lab1.ui;

import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.ui.desktop.MainFrame;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Optional;

public class EditPhoneBookModal extends JDialog {

    private PhoneBookItem savedItem;

    private JTextField firstNameFiled = new JTextField();
    private JTextField lastNameFiled = new JTextField();
    private JTextField patronymicFiled = new JTextField();
    private JTextField phoneField = new JTextField();
    private JButton saveButton = new JButton("Сохранить");
    private final Long id;

    public EditPhoneBookModal(JFrame owner, PhoneBookItem phoneBookItem) {
        super(owner, ModalityType.APPLICATION_MODAL);
        this.id = phoneBookItem.getId();
        this.setResizable(false);
        this.setSize(540, 160);
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        this.setLayout(layout);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;c.weightx = 0.5;
        c.gridy = 0;
        this.add(new JLabel("Фамилия"), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;c.weightx = 0.5;

        c.gridy = 0;
        this.add(lastNameFiled,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;c.weightx = 0.5;

        c.gridy = 1;
        this.add(new JLabel("Имя"),c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;c.weightx = 0.5;

        c.gridy = 1;
        this.add(firstNameFiled,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        this.add(new JLabel("Отчество"),c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        this.add(patronymicFiled,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 3;
        this.add(new JLabel("Телефон"),c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 3;
        this.add(phoneField,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 2;
        c.gridwidth = 2;
        c.gridy = 4;
        this.add(saveButton,c);

        phoneBookItem = Optional.of(phoneBookItem).orElse(new PhoneBookItem());
        firstNameFiled.setText(phoneBookItem.getFirstName());
        lastNameFiled.setText(phoneBookItem.getLastName());
        patronymicFiled.setText(phoneBookItem.getPatronymic());
        phoneField.setText(phoneBookItem.getPhone());

        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                save();
            }
        });

        this.setVisible(true);
    }

    private void save() {
        PhoneBookItem itemToSave = new PhoneBookItem();
        itemToSave.setId(this.id);
        itemToSave.setLastName(lastNameFiled.getText());
        itemToSave.setFirstName(firstNameFiled.getText());
        itemToSave.setPatronymic(patronymicFiled.getText());
        itemToSave.setPhone(phoneField.getText());

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity httpEntity = new HttpEntity<>(itemToSave, httpHeaders);

        this.savedItem = restTemplate.exchange(MainFrame.APP_ENDPOINT, HttpMethod.POST,
                httpEntity,PhoneBookItem.class).getBody();
        this.dispose();
    }

    public PhoneBookItem getSavedItem() {
        return savedItem;
    }
}
