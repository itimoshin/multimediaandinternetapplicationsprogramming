package com.miap.lab1.ui;

import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.ui.desktop.MainFrame;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class PhoneBookTable extends JTable {

    public PhoneBookTable() {
        super(new DefaultTableModel(new PhoneBookItem[][]{}, new String[]{"id", "Фамилия", "Имя", "Отчество", "Телефон"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        this.getColumnModel().removeColumn(this.getColumnModel().getColumn(0));
        PhoneBookTable table = this;
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount()>1) {
                    int row = table.rowAtPoint(e.getPoint());
                    EditPhoneBookModal editPhoneBookModal = new EditPhoneBookModal((JFrame) getRootPane().getParent(), getByIndex(row));
                    editPhoneBookModal.addWindowStateListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                            table.edit(editPhoneBookModal.getSavedItem());
                        }
                    });
                }
            }
        });
        init();
    }

    public void init() {
        ((DefaultTableModel)this.getModel()).setRowCount(0);
        RestTemplate restTemplate = new RestTemplate();
        for (PhoneBookItem phoneBookItem : restTemplate.exchange(MainFrame.APP_ENDPOINT, HttpMethod.GET, null, new ParameterizedTypeReference<List<PhoneBookItem>>() {
        }).getBody()) {
            add(phoneBookItem);
        }
    }

    public void add(PhoneBookItem phoneBookItem) {
        ((DefaultTableModel)this.getModel()).addRow(
                new Object[]{
                        phoneBookItem.getId(),
                        phoneBookItem.getLastName(),
                        phoneBookItem.getFirstName(),
                        phoneBookItem.getPatronymic(),
                        phoneBookItem.getPhone()
                });
    }

    public void edit(PhoneBookItem phoneBookItem) {
        int index = findIndex(phoneBookItem);
        this.getModel().setValueAt(phoneBookItem.getId(),index,0);
        this.getModel().setValueAt(phoneBookItem.getLastName(),index,1);
        this.getModel().setValueAt(phoneBookItem.getFirstName(),index,2);
        this.getModel().setValueAt(phoneBookItem.getPatronymic(),index,3);
        this.getModel().setValueAt(phoneBookItem.getPhone(),index,4);
    }

    public void removeByIndex(int index) {
        new RestTemplate().exchange(MainFrame.APP_ENDPOINT+"/"+getByIndex(index).getId(),HttpMethod.DELETE,null,Void.class);
        init();
    }

    private int findIndex(PhoneBookItem phoneBookItem) {
        for (int i = 0; i < this.getModel().getRowCount(); i++) {
            if(this.getModel().getValueAt(i,0).equals(phoneBookItem.getId())) {
                return i;
            }
        }
        throw new RuntimeException("Phone book item with id="+phoneBookItem+" not found in the table");
    }

    private PhoneBookItem getByIndex(int index) {
        PhoneBookItem phoneBookItem = new PhoneBookItem();
        phoneBookItem.setId((Long) this.getModel().getValueAt(index,0));
        phoneBookItem.setLastName((String) this.getModel().getValueAt(index,1));
        phoneBookItem.setFirstName((String) this.getModel().getValueAt(index,2));
        phoneBookItem.setPatronymic((String) this.getModel().getValueAt(index,3));
        phoneBookItem.setPhone((String) this.getModel().getValueAt(index,4));
        return phoneBookItem;
    }
}
