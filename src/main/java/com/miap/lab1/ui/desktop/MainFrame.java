package com.miap.lab1.ui.desktop;

import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.ui.EditPhoneBookModal;
import com.miap.lab1.ui.PhoneBookTable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainFrame extends JFrame {

    public static final String APP_ENDPOINT = "http://localhost:8080/miap/phoneBook";

    public static void main(String[] args) {
        new MainFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    private final PhoneBookTable table = new PhoneBookTable();

    public MainFrame() throws HeadlessException {
        super("Телефонная книга");
        this.setLayout(new GridBagLayout());
        this.setResizable(false);
        this.setSize(465, 540);
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;c.weightx = 0.5; c.gridy = 0;
        JButton refreshButton = new JButton("Обновить");
        refreshButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                table.init();
            }
        });
        add(refreshButton, c);

        JButton addButton = new JButton("Добавить");
        JFrame window = this;
        addButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                EditPhoneBookModal editPhoneBookModal = new EditPhoneBookModal(window, new PhoneBookItem());
                editPhoneBookModal.addWindowStateListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        table.add(editPhoneBookModal.getSavedItem());
                    }
                });
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;c.weightx = 0.5; c.gridy = 0;
        add(addButton,c);

        JButton removeButton = new JButton("Удалить");
        removeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(table.getSelectedRow()>=0)
                    table.removeByIndex(table.getSelectedRow());
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;c.weightx = 0.5; c.gridy = 0;
        add(removeButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.weightx = 2;
        c.gridwidth = 3;
        c.gridy = 1;
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane,c);
        setVisible(true);
    }
}
