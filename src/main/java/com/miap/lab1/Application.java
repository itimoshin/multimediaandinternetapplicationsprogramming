package com.miap.lab1;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.web.SpringServletContainerInitializer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Arrays;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.miap.lab1"})
@PropertySource("classpath:application.properties")
public class Application extends SpringBootServletInitializer implements ServletContextListener {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext ac = new AnnotationConfigApplicationContext(Application.class);
        sce.getServletContext().setAttribute("applicationContext", ac);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

//    @Bean
//    public ServletRegistrationBean axisServletRegistrationBean() {
//        ServletRegistrationBean registration = new ServletRegistrationBean(new PhoneBookServlet(), "/phoneBook", "/phoneBook/*");
//        return registration;
//    }
}